/**
 * @author Oleh Baidiuk
 * Matrikelnummer: 01468396
 */

public class HausverwaltungClient {

    public static void main(String[] args) {


        Hausverwaltung hvsDao = new Hausverwaltung(args[0]); //save
//        System.out.println("args.length = " + args.length);
        if (args.length > 14) throw new IllegalArgumentException("Error: Parameter ungueltig.");

        try {
            switch (args[1]) {
                case "list":
                    if (args.length > 3) throw new IllegalArgumentException("Arguments ist Falsh!");
                    else if (args.length == 2) {
                        hvsDao.printAll();
                    } else if (args.length == 3)
                        hvsDao.printOneWohnung(Integer.parseInt(args[2]));
                    break;
                case "add":
                    if (args.length < 14) throw new IllegalArgumentException("Error: Parameter ungueltig.");
                    switch (args[2]) {

                        case "EW":
                            EigentumsWohnung ew = new EigentumsWohnung(Integer.parseInt(args[3]),
                                    Double.parseDouble(args[4]), Integer.parseInt(args[5]), Integer.parseInt(args[6]),
                                    Integer.parseInt(args[7]), Integer.parseInt(args[8]), args[9],
                                    Integer.parseInt(args[10]), Integer.parseInt(args[11]),
                                    Double.parseDouble(args[12]), Double.parseDouble(args[13]));
                            hvsDao.add(ew);
                            break;
                        case "MW":
                            MietWohnung mv = new MietWohnung(Integer.parseInt(args[3]), Double.parseDouble(args[4]),
                                    Integer.parseInt(args[5]), Integer.parseInt(args[6]), Integer.parseInt(args[7]),
                                    Integer.parseInt(args[8]), args[9],
                                    Integer.parseInt(args[10]), Integer.parseInt(args[11]),
                                    Double.parseDouble(args[12]), Integer.parseInt(args[13]));
                            hvsDao.add(mv);
                            break;
                    }


                    break;

                case "delete":
                    if (args.length != 3) throw new IllegalArgumentException("Arguments ist Falsh!");
                    hvsDao.del(Integer.parseInt(args[2]));
                    break;

                case "count":
                    if (args.length == 2) System.out.println(hvsDao.getWonAnzahl());
                    else if (args[2].equals("EW")) System.out.println(hvsDao.sizeOfEigentumWohn());
                    else if (args[2].equals("MW")) System.out.println(hvsDao.sizeOfMietWohn());
                    break;
                case "meancosts": //Durchschnittliche monatliche Gesamtkosten aller Wohnungen berechnen
                    System.out.println(hvsDao.priceAvg());


                    break;

                case "oldest":
                    hvsDao.getOldWohId();

                    break;

            }
        } catch (Exception e) {
//            throw new IllegalArgumentException("Error: Parameter ungueltig.");
        }
    }
}