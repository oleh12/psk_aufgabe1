import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author Oleh Baidiuk
 * Matrikelnummer: 01468396
 */

public class Hausverwaltung {

    private HausverwaltungDAO dao;

    public Hausverwaltung(String filePath) {
        dao = new HausverwaltungSerializationDAO(filePath);
    }


    public void printAll() {
        List<Wohnung> wohnungSet = dao.getWohnungen();
//        fahrzeugSet.sort(Comparator.comparingInt(Fahrzeug::getId));
        for (Wohnung w : wohnungSet) System.out.println(w);
    }

    public void printOneWohnung(int i) {
        System.out.println(dao.getWohnungbyId(i));
    }

    public void add(Wohnung w) {
        try {
            dao.saveWohnung(w);
            System.out.println("Info: Wohnung " + w.getId() + " added.");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void del(int i) {
        try {
            dao.deleteWohnung(i);
            System.out.println("Info: Wohnung " + i + " deleted.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getWonAnzahl() {
        List<Wohnung> list = dao.getWohnungen();
        return list.size();
    }

    public int sizeOfEigentumWohn() {
        int counter = 0;
        for (Wohnung w : dao.getWohnungen())
            if (w instanceof EigentumsWohnung) counter++;
        return counter;
    }

    public int sizeOfMietWohn() {
        int counter = 0;
        for (Wohnung w : dao.getWohnungen())
            if (w instanceof MietWohnung) counter++;
        return counter;
    }


    public double priceAvg() {
        double avgPrice;
        double priceSum = 0;
        for (Wohnung w : dao.getWohnungen()) priceSum += w.gesamtKosten();
        avgPrice = priceSum / dao.getWohnungen().size();
        return avgPrice;
    }


    public void getOldWohId() {
        int max_old = 0;
        boolean b = true;
        ArrayList<Wohnung> altList = new ArrayList<>();


        for (Wohnung w : dao.getWohnungen()) {
            if (b) {
                b = false;
                max_old = w.getBaujahr();
            }
            if (w.getBaujahr() < max_old) max_old = w.getBaujahr();
        }

        for (Wohnung w : dao.getWohnungen())
            if (w.getBaujahr() == max_old) System.out.println("Id: " + w.getId());
    }
}