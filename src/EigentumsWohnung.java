import java.text.DecimalFormat;

/**
 * @author Oleh Baidiuk
 * Matrikelnummer: 01468396
 */

public class EigentumsWohnung extends Wohnung {

    private static final long serialVersionUID = 1L;
    public static DecimalFormat df = getDecimalFormat();

    private double betriebskosten; // Betriebskosten (pro m2)
    private double reparaturBeitrag; //  Beitrag für die Reparaturrücklage (pro m2)


    public EigentumsWohnung(int id, double flaeche, int zimmer, int stock, int baujahr,
                            int plz, String strasse, int hausnummer, int top, double betriebskosten, double reparaturBeitrag) throws Exception {
        super(id, flaeche, zimmer, stock, baujahr, plz, strasse, hausnummer, top);
        this.betriebskosten = betriebskosten;
        this.reparaturBeitrag = reparaturBeitrag;
    }

    //    todo Die monatlichen Gesamtkosten ergeben sich bei Eigentumswohnungen aus den
    //    monatlichen Betriebskosten und der Reparaturrücklage zuzüglich eines Zuschlags
    //    Bei einer Eigentumswohnung wird der monatliche Zuschlag in Abhängigkeit vom
    //    Stockwerk berechnet (pro Stockwerk einen Zuschlag von 2%; Erdgeschoß kein
    //    Zuschlag).


    @Override
    double gesamtKosten() {
        if (getStock() == 0) return (betriebskosten + reparaturBeitrag) * getFlaeche();
        double sum = (betriebskosten + reparaturBeitrag) * getFlaeche();
        return sum * (1 + (getStock() * 0.02));
    }
    //todo
    @Override
    public String toString() {
        return "Typ:            EW" + '\n' +
                "Id:             " + getId() + '\n' +
                "Flaeche:        " + df.format(getFlaeche()) + '\n' +
                "Zimmer:         " + getZimmer() + '\n' +
                "Stock:          " + getStock() + '\n' +
                "Baujahr:        " + getBaujahr() + '\n' +
                "PLZ:            " + getPlz() + '\n' +
                "Strasse:        " + getStrasse() + '\n' +
                "Hausnummer:     " + getHausnummer() + '\n' +
                "Top:            " + getTop() + '\n' +
                "Betriebskosten: " + df.format(betriebskosten) + '\n' +
                "Ruecklage:      " + df.format(reparaturBeitrag) + "\n";
    }
}