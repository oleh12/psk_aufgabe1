/**
 * @author Oleh Baidiuk
 * Matrikelnummer: 01468396
 */

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;


public abstract class Wohnung implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private double flaeche;
    private int zimmer;
    private int stock;
    private int baujahr;
    private int plz;
    private String strasse;
    private int hausnummer;
    private int top;

    public static int currentYear = Calendar.getInstance().get(Calendar.YEAR);

    public static DecimalFormat getDecimalFormat() {
        DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance();
        dfs.setDecimalSeparator('.');
        return new DecimalFormat("0.00", dfs);
    }

    public Wohnung(int id, double flaeche, int zimmer, int stock, int baujahr,
                   int plz, String strasse, int hausnummer, int top) throws Exception {
        setId(id);
        setFlaeche(flaeche);
        setZimmer(zimmer);
        setStock(stock);
        setBaujahr(baujahr);
        this.plz = plz;
        this.strasse = strasse;
        this.hausnummer = hausnummer;
        this.top = top;
    }


    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public double getFlaeche() {
        return flaeche;
    }
    public void setFlaeche(double flaeche) {
        this.flaeche = flaeche;
    }

    public int getZimmer() {
        return zimmer;
    }
    public void setZimmer(int zimmeranzahl) {
        this.zimmer = zimmeranzahl;
    }

    public int getStock() {
        return stock;
    }
    public void setStock(int stockwerk) {
        this.stock = stockwerk;
    }

    public int getBaujahr() {
        return baujahr;
    }

    public void setBaujahr(int baujahr) {
        if (baujahr > currentYear) throw new IllegalArgumentException("Error: Baujahr ungueltig.");
        this.baujahr = baujahr;
    }
    public int getPlz() {
        return plz;
    }

    public void setPlz(int plz) {
        this.plz = plz;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public int getHausnummer() {
        return hausnummer;
    }

    public void setHausnummer(int hausnummer) {
        this.hausnummer = hausnummer;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }


    public int alter() {
        return currentYear - baujahr;
    }

    abstract double gesamtKosten(); //todo

}