import java.text.DecimalFormat;

/**
 * @author Oleh Baidiuk
 * Matrikelnummer: 01468396
 */

public class MietWohnung extends Wohnung {

    private static final long serialVersionUID = 1L;
    public static DecimalFormat df = getDecimalFormat();


    private double monatMiete; // Instanzvariablen für die monatlichen Mietkosten (pro m2)
    private int mieterAnz; //Anzahl der Mieter einer Wohnung.

    public MietWohnung(int id, double flaeche, int zimmer, int stock, int baujahr,
                       int plz, String strasse, int hausnummer, int top, double monatMiete, int mieterAnz) throws Exception {

        super(id, flaeche, zimmer, stock, baujahr, plz, strasse, hausnummer, top);

        this.monatMiete = monatMiete;
        this.mieterAnz = mieterAnz;
    }

    //todo Die monatlichen Gesamtkosten ergeben sich bei
    //Mietwohnungen aus den monatlichen Mietkosten und ebenfalls zuzüglich eines Zuschlags.
    //Bei Mietwohnung wird der Zuschlag abhängig von der Anzahl der Mieter
    //berechnet - kein Zuschlag bei einem Mieter, jeweils 2,5% für jeden weiteren Mieter –
    //wobei der maximal mögliche Zuschlag 10% beträgt.
    @Override
    double gesamtKosten() {
        if (mieterAnz == 1) return monatMiete * getFlaeche();
        if (mieterAnz >= 10) return monatMiete * 1.25 * getFlaeche();
        return monatMiete * getFlaeche() * (mieterAnz * 0.025 + 1);
    }

    //todo
    @Override
    public String toString() {
        return "Typ:            MW" + '\n' +
                "Id:             " + getId() + '\n' +
                "Flaeche:        " + df.format(getFlaeche()) + '\n' +
                "Zimmer:         " + getZimmer() + '\n' +
                "Stock:          " + getStock() + '\n' +
                "Baujahr:        " + getBaujahr() + '\n' +
                "PLZ:            " + getPlz() + '\n' +
                "Strasse:        " + getStrasse() + '\n' +
                "Hausnummer:     " + getHausnummer() + '\n' +
                "Top:            " + getTop() + '\n' +
                "Miete/m2:       " + df.format(monatMiete) + '\n' +
                "Anzahl Mieter:  " + mieterAnz + "\n";


    }
}