import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Oleh Baidiuk
 * Matrikelnummer: 01468396
 */

public class HausverwaltungSerializationDAO implements HausverwaltungDAO {

    private String filePath;
    private ArrayList<Wohnung> wohnungsList;


    private static HausverwaltungSerializationDAO instance;

    private HausverwaltungSerializationDAO() {
    }  //singelton


    public HausverwaltungSerializationDAO(String filePath) {
        this.filePath = filePath;
        restoreData();
    }

    public static HausverwaltungSerializationDAO getInstance() {
        if (instance == null) instance = new HausverwaltungSerializationDAO();
        return instance;
    }


    private void restoreData() {
        File file = new File(filePath);
        if (file.exists())
            try {
                FileInputStream fileInputStream = new FileInputStream(filePath);
                ObjectInputStream os = new ObjectInputStream(fileInputStream);
                wohnungsList = (ArrayList<Wohnung>) os.readObject();
                os.close();
                fileInputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(1);
            }
        else {
            wohnungsList = new ArrayList<>();
        }
    }

    private void saveData() {

        try {
            File outFile = new File(filePath);
            if (outFile.getParentFile() != null) outFile.getParentFile().mkdirs();

            FileOutputStream fileOutputStream = new FileOutputStream(filePath);
            ObjectOutputStream os = new ObjectOutputStream(fileOutputStream);
            os.writeObject(wohnungsList);
            os.close();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }


    @Override
    public List<Wohnung> getWohnungen() {
        return wohnungsList;
    }

    @Override
    public Wohnung getWohnungbyId(int id) {
        for (Wohnung wohnung : wohnungsList)
            if (wohnung.getId() == id) return wohnung;

        return null;
    }

    @Override
    public void saveWohnung(Wohnung wohnung) {

        for (Wohnung w : wohnungsList)
            if (wohnung.getId() == w.getId())
                throw new IllegalArgumentException("Error: Wohnung bereits vorhanden. (id=<" + wohnung.getId()+">)");

        wohnungsList.add(wohnung);
        saveData();
    }

    @Override
    public void deleteWohnung(int id) {
        Wohnung wohnung = null;

        for (Wohnung w : wohnungsList)
            if (w.getId() == id)
                wohnung = w;

        if (wohnung == null)
            throw new IllegalArgumentException("Error: Wohnung nicht vorhanden. (id=<"+id+">)");


        wohnungsList.remove(wohnung);
        saveData();
    }
}